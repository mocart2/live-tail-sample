#!/bin/bash

infinite_error_spy(){
    tail -f -n 0 $log_file_path |
    while read line
    do
        case $line in
            *"$search_string"*)
            echo "Error detected restarting service"
                if systemctl restart rsyslog
                then
                echo "service is restarted"
                else
                echo "service is NOT restarted"
                exit 2
                fi
            ;;
        esac
    done
}


if [ -z "$1" ]
then
    echo "You do not provide conf file or provide not correct path to conf file. Please check it." | systemd-cat -p err
    exit 2
else
    if [ -f "$1" ]; then
        source $1
        infinite_error_spy $log_file_path $search_string
    else
        echo "config file $1 is NOT EXISTS!"  | systemd-cat -p err
        exit 2
    fi
fi
