# live-tail-sample
### copy files:

all steps must be run as `root` user

1. `mkdir -p /opt/live-tail-sample/conf`
2. `cp ./syslog.conf /opt/live-tail-sample/conf/syslog.conf`
3. `cp ./live-tail-sample.sh /opt/live-tail-sample`
4. `chmod +x /opt/live-tail-sample/live-tail-sample.sh`
5. `cp ./backend@.service /etc/systemd/system/backend@.service`
6. `systemctl daemon-reload`
7. `systemctl enable backend@.service`
8. `systemctl start backend@syslog.service`


Send (generate) 10 test error messages:

```shell
for i in {1..10}; do
echo "Dec 27 00:01:30 ip-10-0-5-224 worker-tasks: [2019-12-27 00:01:30,710: ERROR/MainProcess] Error in timer: TimeoutError('Timeout reading from socket',)\nTraceback (most recent call last):\n" >> /var/log/syslog && sleep 3
done
```

if daemon find errors in log file from config, consists `Timeout reading from socket` it will be restart some service (in example `systemctl restart rsyslog`)
